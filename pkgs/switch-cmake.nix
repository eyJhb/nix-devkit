{
  src,
  stdenv,
  fetchFromGitHub,
  # automake,
  # autoconf
}:

stdenv.mkDerivation {
  name = "switch-cmake";
  version = "1.0.0";

  src = src;

  phases = [ "unpackPhase" "installPhase" ];

  installPhase = ''
    # TODO(eyJhb): tmp remove this
    install -D cmake/switch/aarch64-none-elf-cmake $out/bin/aarch64-none-elf-cmake

    install -D cmake/switch/aarch64-none-elf-cmake $out/portlibs/switch/bin/aarch64-none-elf-cmake
    install -D cmake/switch/Switch.cmake $out/cmake/Switch.cmake
    install -D cmake/switch/NintendoSwitch.cmake $out/cmake/Platform/NintendoSwitch.cmake
  '';
}
