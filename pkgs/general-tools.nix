{
  stdenv,
  fetchFromGitHub,
  automake,
  autoconf
}:

stdenv.mkDerivation rec {
  name = "general-tools";
  version = "1.2.0";

  src = fetchFromGitHub {
    owner = "devkitPro";
    repo = name;
    rev = "v${version}";
    sha256 = "sha256-n8JHQiI83oka/WOkh5Uvg/n9DFf7F+Ra9z1lbpGcpz0=";
  };

  nativeBuildInputs = [ autoconf automake ];

  preConfigure = ''
    sh autogen.sh
  '';
}
