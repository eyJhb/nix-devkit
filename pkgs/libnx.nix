{
  stdenv,
  fetchFromGitHub,
  symlinkJoin,
  devkit-gcc,
  general-tools,
  devkita64-rules
}:

stdenv.mkDerivation rec {
  name = "libnx";
  version = "4.2.0";

  src = fetchFromGitHub {
    owner = "switchbrew";
    repo = "libnx";
    rev = "v${version}";
    sha256 = "sha256-m5LIenMWTJ4JyfjjYftEUBbrC33gvZeRlVaYmzZTfFI=";
  };

  DEVKITPRO = symlinkJoin {
    name = "devkitPro-libnx";
    paths = [
      devkita64-rules
    ];
  };

  installPhase = ''
    mkdir -p $out/libnx
    cp nx/default_icon.jpg $out/libnx
    cp -a nx/include $out/libnx
    cp -a nx/lib $out/libnx
    cp nx/switch* $out/libnx
  '';

  buildInputs = [ devkit-gcc general-tools ];
}
