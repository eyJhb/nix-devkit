{
  pkgs
  , devkit-gcc
}:

let
  nixpkgs = builtins.fetchTarball {
    url    = "https://github.com/NixOS/nixpkgs/archive/296e8fef618c7597c3dfa84be2fcdb2771350b61.tar.gz";
    sha256 = "sha256:1g5q5nd783q1ifcl0b5r653k5ys9hh0ig635sng7w2vcbcp1ysp7";
  };

  oldpkgs = import nixpkgs {};

  # pkgsCross = oldpkgs.pkgsCross.aarch64-embedded.buildPackages;
  pkgsCross = pkgs.pkgsCross.aarch64-embedded.buildPackages;

  oversion = "20.1.0-rc3";
in pkgsCross.mesa.overrideAttrs (oldAttrs: rec {
  version = oversion;
  src = pkgs.fetchFromGitHub {
    owner = "devkitPro";
    repo = "mesa";
    rev = "switch-${oversion}";
    sha256 = "sha256-1Huo6ggfkvZ2oj8PvOROyOi97F3nHtIV1attBLfLM9M=";
  };

  # nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [
  #   pkgsCross.bison
  #   pkgsCross.flex
  #   devkit-gcc
  # ];

  configureFlags = [];

  # patches = [];
})
