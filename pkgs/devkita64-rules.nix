{
  stdenv,
  fetchFromGitHub
}:

stdenv.mkDerivation rec {
  name = "devkita64-rules";
  version = "1.0.1";

  src = fetchFromGitHub {
    owner = "devkitPro";
    repo = name;
    rev = "v${version}";
    sha256 = "sha256-CxjVHA6+GpnK8yV2Z7dOTa3cHoA5f/dXCatV26vvWSQ=";
  };

  phases = [ "unpackPhase" "installPhase" ];

  installPhase = ''
    install -D base_rules $out/devkitA64/base_rules
    install -D base_tools $out/devkitA64/base_tools
  '';
}
