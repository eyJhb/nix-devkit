{
  stdenv,
  fetchFromGitHub,
  meson,
  bison,
  flex,
  ninja
}:

# { lib, stdenv, meson, ninja, fetchFromGitHub, glib, pkg-config, gtk-doc, docbook_xsl, gobject-introspection }:

stdenv.mkDerivation rec {
  pname = "dekotools";
  version = "unstable-20220314";

  src = fetchFromGitHub {
    owner = "fincs";
    repo = "dekotools";
    rev = "aebf6e299383668ff1d337ad6cb3daca0d4c3754";
    sha256 = "sha256-Ciqs/55Ne/3jefgHxOkc4Ba5j2QMiBvZPGzoRyYrU0Q=";
  };

  nativeBuildInputs = [ meson ];
  buildInputs = [ bison flex ninja ];

  # mesonFlags = [ "-Dbash-completions=true" ];

}
