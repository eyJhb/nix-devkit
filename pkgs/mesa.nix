{
  stdenvNoCC,
  fetchurl,
  python3,
  pkg-config,
  cmake,
  meson

  , devkitPro
  , devkit-gcc
}:

# { lib, stdenv, meson, ninja, fetchFromGitHub, glib, pkg-config, gtk-doc, docbook_xsl, gobject-introspection }:

stdenvNoCC.mkDerivation rec {
  name = "mesa";
  version = "20.1.0-rc3";

  DEVKITPRO = devkitPro;

  # flags test
  PORTLIBS_PREFIX="${DEVKITPRO}/portlibs/switch";
  ARCH="-march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft -fPIC -ftls-model=local-exec";
  # ARCH="-march=armv8-a+crc+crypto -mtune=cortex-a57 -fPIC -ftls-model=local-exec";
  CFLAGS="${ARCH} -O2 -ffunction-sections -fdata-sections";
  CXXFLAGS="${CFLAGS}";
  CPPFLAGS="-D__SWITCH__ -I${PORTLIBS_PREFIX}/include -isystem ${DEVKITPRO}/libnx/include";
  LDFLAGS="${ARCH} -L${PORTLIBS_PREFIX}/lib -L${DEVKITPRO}/libnx/lib";
  LIBS="-lnx";


  src = fetchurl {
    url = "https://mesa.freedesktop.org/archive/mesa-${version}.tar.xz";
    sha256 = "sha256-yQt16jQwLr3puBuHxWQvqGTED+nErTTOB5MXDBQTFo0=";
  };
  patches = [ ./../patches/switch-mesa-20.1.0-4.patch ];

  nativeBuildInputs = [
    devkit-gcc

    meson
    python3
  ];
  buildInputs = nativeBuildInputs;

  mesonFlags = [
    # "--buildtype=plain"
    # "--default-library=static"
  ];
}
