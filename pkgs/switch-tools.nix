{
  fetchFromGitHub,
  stdenv,
  automake,
  autoconf,
  pkg-config,
  lz4,
  zlib
}:

stdenv.mkDerivation rec {
  name = "switch-tools";
  version = "1.10.0";

  src = fetchFromGitHub {
    owner = "switchbrew";
    repo = "switch-tools";
    rev = "v${version}";
    sha256 = "sha256-igg1Tm7m5RWSt9M/wrHJPQVVuiGx8jJg/zaJbHYdAr0=";
  };

  nativeBuildInputs = [ automake autoconf lz4 zlib pkg-config ];

  preConfigure = ''
    sh autogen.sh
  '';
}
