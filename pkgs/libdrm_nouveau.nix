{
  stdenv
, fetchFromGitHub
, symlinkJoin
, devkit-gcc
, devkita64-rules
, libnx
}:

stdenv.mkDerivation rec {
  name = "switch-libdrm_nouveau";
  version = "1.0.1";

  src = fetchFromGitHub {
    owner = "devkitPro";
    repo = "libdrm_nouveau";
    rev = "v${version}";
    sha256 = "sha256-OeJ41o2vzg9+XHL8ksK5ACTQ6h6TQ+mPNIhJ3GnRDG0=";
  };

  DEVKITPRO = symlinkJoin {
    name = "devkitPro-libnx";
    paths = [
      devkita64-rules
      libnx
    ];
  };

  installPhase = ''
    mkdir -p $out
    DESTDIR=$out make install
  '';

  buildInputs = [ devkit-gcc ];
}
