{ pkgs }:

let
  # srcs = import ./srcs.nix {};
  # pkgs = import srcs.nixpkgs {};
  lib = pkgs.lib;

  # sources
  newlibSrc = pkgs.fetchFromGitHub {
    owner = "devkitPro";
    repo = "newlib";
    rev = "7d46b83a8ee9e95fe40db0844d147a7f4e6fea3a";
    sha256 = "sha256-P0/lxVX6iS2wYXS4YwR8yZ1adHF5vAXmiHWKK60C0w8=";
  };

  # override gcc11
  gccOverride = pkgs.gcc11.cc.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches ++ [ ./../patches/gcc-11.2.0.patch ];

    CFLAGS_FOR_TARGET="-O2 -ffunction-sections -fdata-sections";
    CXXFLAGS_FOR_TARGET="-O2 -ffunction-sections -fdata-sections";
    configureFlags =
      (lib.filter (x: if !lib.isString x
                      then true
                      else !(lib.hasPrefix "--with-headers=" x
                             || lib.hasPrefix "--with-ld=" x
                             || lib.hasPrefix "--with-as=" x
                             || lib.hasPrefix "--disable-multilib" x
                      )) oldAttrs.configureFlags)
        # we need to override ld and as here as well!!!
      ++ [
        "--with-headers=${newlibSrc}/newlib/libc/include"
        "--with-ld=${binutilsWrapped}/bin/aarch64-none-elf-ld"
        "--with-as=${binutilsWrapped}/bin/aarch64-none-elf-as"
      ];
  });

  gccWrapped = pkgs.wrapCCWith {
    cc = gccOverride;
    bintools = binutilsWrapped;
  };

  newlibOverride = pkgs.newlib.overrideAttrs (oldAttrs: rec {
    version = "4.2.0.20211231";

    src = newlibSrc;

    # add gccOverride to provide aarch64-none-elf-gcc
    CFLAGS_FOR_TARGET = "-O2 -ffunction-sections -fdata-sections";
    configureFlags = [
      "--disable-newlib-supplied-syscalls"
      "--enable-newlib-mb"
      "--disable-newlib-wide-orient"
    ];

    buildInputs = [
      gccWrapped
    ];
  });

  binutilsOverride = pkgs.binutils-unwrapped.overrideAttrs (oldAttrs: rec {
    version = "2.32";

    src = pkgs.fetchurl {
      url = "https://sourceware.org/pub/binutils/releases/binutils-${version}.tar.bz2";
      sha256 = "sha256-3jixXJAusnJerGryEYOl806kY0ywvO8ZYStQ5e0xBy0=";
    };

    configureFlags = [
      "--disable-nls"
      "--disable-werror"
      "--enable-lto"
      "--enable-plugins"
      "--enable-poison-system-directories"
    ];
    patches = [ ./../patches/binutils-2.32.patch ];
  });

  binutilsWrapped = pkgs.wrapBintoolsWith {
    bintools = binutilsOverride;
  };
in {
  devkit-gcc = gccWrapped;
  devkit-gcc-unwrapped = gccOverride;

  devkit-binutils = binutilsWrapped;
  devkit-binutils-unwrapped = binutilsOverride;

  devkit-newlib = newlibOverride;
}
