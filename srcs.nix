{ pkgs ? import <nixpkgs> {}, ... }:

{
  nixpkgs = pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "3e072546ea98db00c2364b81491b893673267827";
    sha256 = "sha256-8nQx02tTzgYO21BP/dy5BCRopE8OwE8Drsw98j+Qoaw=";
  };
  pacman-packages = pkgs.fetchFromGitHub {
    owner = "devkitPro";
    repo = "pacman-packages";
    rev = "ef87c316edf3df5b44e957d628b5fac276e9a1d8";
    sha256 = "sha256-eHxPDhsOPj/3aAK23/gWqwpGBUioQ8QjhvvbWKPDBA4=";
  };
}
