{
  fetchFromGitHub,
  stdenv,
  devkitPro,
  switch-tools,
  devkit-gcc
}:

let
  version = "20201219";

  src = fetchFromGitHub {
    owner = "switchbrew";
    repo = "switch-examples";
    rev = "v${version}";
    sha256 = "sha256-oVRgvOOHKbu/DsS3UhNuLN51MTbwlXDoe6DlK9+cYgY=";
  };

in stdenv.mkDerivation {
  name = "switch-examples-templates-application";
  version = version;

  src = "${src}/templates/application";

  buildInputs = [ devkit-gcc switch-tools ];

  DEVKITPRO = devkitPro;

  installPhase = ''
    install -D application.nro $out/application.nro
  '';
}
