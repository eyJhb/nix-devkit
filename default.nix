{ pkgs ? import <nixpkgs> {}, ... }:

let
  srcs = import ./srcs.nix {};
  pkgsReal = import srcs.nixpkgs {};
  pkgs = (import srcs.nixpkgs {}).pkgsCross.aarch64-embedded.buildPackages;
  lib = pkgs.lib;

  # custom callPackage
  callPackage = path: overrides: let
    f = import path;
    allPkgs = pkgs // tools // libs // toolchain // { devkitPro = devkitpro; };
  in f ((builtins.intersectAttrs (builtins.functionArgs f) allPkgs) // overrides);

  toolchain = pkgs.callPackage ./pkgs/toolchain.nix {};

  tools = {
    switch-tools = callPackage ./pkgs/switch-tools.nix {};
    general-tools = callPackage ./pkgs/general-tools.nix {};
    dekotools = callPackage ./pkgs/dekotools.nix { };

    switch-cmake = callPackage ./pkgs/switch-cmake.nix { src = srcs.pacman-packages; };
    devkita64-rules = callPackage ./pkgs/devkita64-rules.nix {};
  };

  libs = rec {
    libnx = callPackage ./pkgs/libnx.nix {};
    libdrm_nouveau = callPackage ./pkgs/libdrm_nouveau.nix {};

    mesa = callPackage ./pkgs/mesa.nix {};

    mesa2 = pkgs.callPackage ./pkgs/mesa2.nix {
      devkit-gcc = toolchain.devkit-gcc;
    };
  };

  apps = {
    application = callPackage ./apps/switch-examples/default.nix {
      # devkitPro = devkitpro2;
    };
  };


  # devkitpro
  devkitpro = pkgs.symlinkJoin {
    name = "devkitpro";
    paths = [
      libs.libnx

      tools.general-tools

      tools.switch-cmake
      tools.devkita64-rules

      (pkgs.runCommand "newlib" {} ''
        mkdir -p $out/devkitA64
        mkdir -p $out/libnx/lib
        cp -a ${toolchain.devkit-newlib}/. $out/devkitA64

        # HACK, it NEEDS PIC, and it cannot find it the correct place
        cp -a ${toolchain.devkit-newlib}/aarch64-none-elf/lib/pic/. $out/libnx/lib
      '')
    ];
  };

  devkitproShell = pkgs.mkShell {
    buildInputs = [
      toolchain.devkit-gcc
      libs.switch-tools
    ];
  
    shellHook = ''
      export PATH=${devkitpro}/bin:${toolchain.devkit-binutils}/bin:$PATH
      export DEVKITPRO=${devkitpro}
      echo $DEVKITPRO
    '';
  };

  testShell = pkgs.mkShell {
    buildInputs = [
      toolchain.devkit-gcc
      # toolchain.devkit-binutils

      # toolchain.devkit-gcc-unwrapped
      # toolchain.devkit-binutils-unwrapped
    ];
  };
  
in {
  inherit libs apps toolchain;
  shell = devkitproShell;
}
# in libs
# in devkitproShell
# in apps.application
# in libs.mesa
# in libs.switch-tools
# in testShell
# in toolchain
# in mkDevkitPro libs
# in libs.dekotools
# in libs.test
# in libs.devkita64-rules
# in libs.general-tools
# in libs.switch-cmake
